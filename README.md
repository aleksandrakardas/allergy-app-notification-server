# Server for Firebase Cloud Messaging Notifications
### Installation

* Clone project `git clone git@gitlab.com:aleksandrakardas/allergy-app-notification-server.git`
* Install node.js (min. version 16+) and check version with `node -v``
* Go to functions directory `cd functions/` and Run `npm install` in terminal
* Run `npm install -g firebase-tools` in terminal
* Run `firebase login --interactive` in terminal and login to Julka google account


### Testing

* Run `firebase emulators:start` to test functions
* Go to domain: `http://127.0.0.1:4000/logs?q=metadata.emulator.name%3D%22functions%22``
* Trigger pollution function: `http://127.0.0.1:5001/zpoproject-bcbf6/us-central1/sendPollutionNotificationHttps`
* Trigger pollen function: `http://127.0.0.1:5001/zpoproject-bcbf6/us-central1/sendPollenNotificationHttps`
