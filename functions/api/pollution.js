const fetch = require("node-fetch");

exports.fetchPollutionData = async () => {
  const airResponse = await fetch("https://api.airvisual.com/v2/nearest_city?lat=51&lon=17&key=f090aca4-829d-488a-959c-9730aad29faa");
  const owmResponse = await fetch("https://api.openweathermap.org/data/2.5/air_pollution?lat=51&lon=17&appid=3011f69e0ed1581e2e1f43c936adc070");

  const airData = await airResponse.json();
  const owmData = await owmResponse.json();

  return `
        ${getAqi(airData)}
        ${getPM25(owmData)}
        ${getPM10(owmData)}
    `;
};

function getAqi(data) {
  const aqi = data.data.current.pollution.aqius;
  const status = normStatus(Number(aqi), 51, 101, 201, 301);
  return `Aqi: \t${aqi}\t${status}`;
}

function getPM25(data) {
  const pm = data.list[0].components.pm2_5;
  const status = normStatus(Number(pm), 31, 61, 91, 121);
  return `PM2.5: \t${pm}\t${status}`;
}

function getPM10(data) {
  const pm = data.list[0].components.pm10;
  const status = normStatus(Number(pm), 51, 101, 251, 351);
  return `PM10: \t${pm}\t${status}`;
}

function normStatus(value, norm1, norm2, norm3, norm4) {
  if (value < norm1) return "Good";
  else if (value < norm2) return "Moderate";
  else if (value < norm3) return "Unhealthy";
  else if (value < norm4) return "Very unhealthy";
  else return "Hazardous";
}
