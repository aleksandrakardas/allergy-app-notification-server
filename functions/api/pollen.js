const fetch = require("node-fetch");

exports.fetchPollenData = async () => {
    const response = await fetch("https://api.ambeedata.com/latest/pollen/by-lat-lng?lat=51&lng=17&speciesRisk=true", {
        headers: {
            "Content-Type": "application/json",
            "x-api-key": "38d157d748abddc100073fab0795e705bf19635781088edc1a2830eb683e34d9"
        }
    });
    const data = await response.json();

    return `
        ${getData(data, "Alder", "Tree", "Alder")}
        ${getData(data, "Birch", "Tree", "Birch")}
        ${getData(data, "Chenopod", "Weed", "Chenopod")}
        ${getData(data, "Elm", "Tree", "Elm")}
        ${getData(data, "Grass / Poaceae", "Grass", "Grass")}
        ${getData(data, "Hazel", "Tree", "Hazel")}
        ${getData(data, "Mugwort", "Weed", "Mugwort")}
        ${getData(data, "Nettle", "Weed", "Nettle")}
        ${getData(data, "Oak", "Tree", "Oak")}
        ${getData(data, "Pine", "Tree", "Pine")}
        ${getData(data, "Plane", "Tree", "Plane")}
        ${getData(data, "Poplar / Cottonwood", "Tree", "Poplar")}
        ${getData(data, "Ragweed", "Weed", "Ragweed")}
    `
};

function getData(data, key, valueKey, name) {
    const value = data.data[0].Species[valueKey][key];
    const level = data.data[0].SpeciesRisk[key];

    return `${name}: \t${value}\t${level}`;
}