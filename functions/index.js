const admin = require("firebase-admin");
const {
  sendPollutionNotificationDaily,
  sendPollenNotificationDaily,
} = require("./dailyNotifications");

const {
  sendPollutionNotificationHttps,
  sendPollenNotificationHttps,
} = require("./httpsNotifications");

admin.initializeApp();

exports.sendPollutionNotificationDaily = sendPollutionNotificationDaily(admin);
exports.sendPollenNotificationDaily = sendPollenNotificationDaily(admin);

exports.sendPollutionNotificationHttps = sendPollutionNotificationHttps(admin);
exports.sendPollenNotificationHttps = sendPollenNotificationHttps(admin);
