const functions = require("firebase-functions");
const { fetchPollenData } = require("./api/pollen");
const {fetchPollutionData} = require("./api/pollution");

exports.sendPollutionNotificationDaily = (admin) => functions.pubsub.schedule("0 9 * * *")
    .timeZone("Poland")
    .onRun(async () => {
      const message = await fetchPollutionData();
      const payload = {
        topic: "pollution",
        notification: {
          title: "Today's Pollution status:",
          body: message,
        },
        data: {
          body: message,
        },
      };

      admin.messaging().send(payload)
          .then((response) => {
            // Response is a message ID string.
            console.log("Successfully sent message:", response);
            return {success: true};
          })
          .catch((error) => {
            return {error: error.code};
          });
    });

exports.sendPollenNotificationDaily = (admin) => functions.pubsub.schedule("0 9 * * *")
    .timeZone("Poland")
    .onRun(async () => {
      const message = await fetchPollenData();
      const payload = {
        topic: "pollen",
        notification: {
          title: "Today's Pollen status:",
          body: message,
        },
        data: {
          body: message,
        },
      };

      admin.messaging().send(payload)
          .then((response) => {
            // Response is a message ID string.
            console.log("Successfully sent message:", response);
            return {success: true};
          })
          .catch((error) => {
            return {error: error.code};
          });
    });
