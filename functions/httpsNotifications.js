const functions = require("firebase-functions");
const {fetchPollutionData} = require("./api/pollution");
const { fetchPollenData } = require("./api/pollen");

exports.sendPollutionNotificationHttps = (admin) => functions.https.onRequest(async (req, res) => {
  const message = await fetchPollutionData();
  const payload = {
    topic: "pollution",
    notification: {
      title: "Today's Pollution status:",
      body: message,
    },
    data: {
      body: message,
    },
  };

  admin.messaging().send(payload)
      .then((response) => {
        // Response is a message ID string.
        console.log("Successfully sent message:", response);
        return {success: true};
      })
      .catch((error) => {
        return {error: error.code};
      })
      .finally(() => {
        res.end();
      });
});

exports.sendPollenNotificationHttps = (admin) =>
  functions.https.onRequest(async (req, res) => {
    const message = await fetchPollenData();

    const payload = {
      topic: "pollen",
      notification: {
        title: "Today's Pollen status:",
        body: message,
      },
      data: {
        body: message,
      },
    };

    admin.messaging().send(payload)
        .then((response) => {
          // Response is a message ID string.
          console.log("Successfully sent message:", response);
          return {success: true};
        })
        .catch((error) => {
          return {error: error.code};
        })
        .finally(() => {
          res.end();
        });
  });
